<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Biblioteca</title>
    </head>
    <body>
        <nav>
            <ul style = "text-align:center" class="menu">
                <li class="item"><a href="/">Home</a></li>
                <li class="item"><a href="/biblioteca">Livros</a></li>
                <li class="item"><a href="/autores">Autores</a></li>
                <li class="item"><a href="/editora">Editoras</a></li>
                
            </ul>
        </nav>
        <nav>
            <ul style = "text-align:center" class="menu">
                <li class="item"><a href="/editarlivros">Editar livros</a></li>
                <li class="item"><a href="/editarautores">Editar autores</a></li>
                <li class="item"><a href="/editareditoras">Editar editoras</a></li>  
            </ul>
        </nav>
        <hr color="black">
        <div border='4'>
            <h1  style = "text-align:center">Listagem das Autores</h1>
            <table border='1' width="500" align="center">
                <tr>
                    <td  class="tab1" width="500">
                        <h4>ID do Autor:</h4>
                    </td>
                    <td  class="tab1" width="500">
                        <h4>Nome do Autor:</h4>
                    </td>
                </tr>
                @foreach($autor as $autor)
                <tr>
                    <td  class="tab" width="500">
                        {{$autor->id}}
                    </td>
                    <td  class="tab" width="500">
                        {{$autor->autor}}
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </body>
</html>