<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Biblioteca da Ana</title>
    </head>
    <body>
        <nav>
            <ul style = "text-align:center" class="menu">
                <li><a href="/">Home</a></li>
                <li><a href="/biblioteca">Livros</a></li>
                <li><a href="/autores">Autores</a></li>
                <li><a href="/editora">Editoras</a></li>
            </ul>
        </nav>
        <nav>
            <ul style = "text-align:center">
            <li><a href="/editarlivros">Editar livros</a></li>
            <li><a href="/editarautores">Editar autores</a></li>
                <li class="item"><a href="/editareditoras">Editar editoras</a></li>  
            </ul>
        </nav>
        <hr color="black">
        <h1 style = "text-align:center">Pagina Inicial</h1>
        <div>
            <h4  style = "text-align:center" >Clique para ter acesso a Biblioteca</h4>
        </div>
        <a href="/biblioteca"> 
        <button>Biblioteca</button></a>
    </body>
</html>

