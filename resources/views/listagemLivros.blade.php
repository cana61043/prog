<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Biblioteca</title>
    </head>
    <body>
        <nav>
            <ul style = "text-align:center">
                <li><a href="/">Home</a></li>
                <li><a href="/biblioteca">Livros</a></li>
                <li><a href="/autores">Autores</a></li>
                <li><a href="/editora">Editoras</a></li>
                
            </ul>
        </nav>
        <nav>
            <ul style = "text-align:center">
                <li><a href="/editarlivros">Editar livros</a></li>
                <li><a href="/editarautores">Editar autores</a></li>
                <li><a href="/editareditoras">Editar editoras</a></li>  
            </ul>
        </nav>
        <hr color="black">
        <div border='4'>
            <h1  style = "text-align:center">Listagem dos Livros</h1>
            <table  border='1' width="500" align="center">
                <tr>
                    <td class="tab1" width="500">
                        <h4> ID do livro:</h4> 
                    </td>
                    <td  class="tab1" width="500">
                        <h4>Título do livro:</h4>
                    </td>
                    <td  class="tab1" width="500"> 
                        <h4>ID do autor:</h4>
                    </td>
                    <td  class="tab1" width="500"> 
                        <h4>ID da editora:</h4>
                    </td>
                </tr>
                @foreach($livro as $livro)
                <tr>
                    <td class="tab" class="tab" width="500">
                        {{$livro->id}}
                    </td>
                    <td class="tab" width="500">
                        {{$livro->livro}}
                    </td>
                    <td  class="tab" width="500">
                        {{$livro->autor}}
                    </td>
                    <td class="tab" width="500">
                        {{$livro->editora}}
                    </td>
                </tr>
                @endforeach
            </table>
    </body>
</html>
